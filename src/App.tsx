import React, { ChangeEvent, useState } from 'react';
import logo from './logo.svg';
import './App.css';
import HomePage from './pages/HomePage';
import InboxPage from './pages/InboxPage';
import AboutPage from './pages/AboutPage';
import NotFoundPage from './pages/NotFoundPage';
import PostDetailPage from './pages/PostDetailPage';

function App() {
  const [page, setPage] = useState('home');
  let content;
  switch (page) {
    case 'home':
      content = <HomePage setPage={setPage} />;
      break;
    case 'inbox':
      content = <InboxPage />;
      break;
    case 'about':
      content = <AboutPage />;
      break;
    default:
      if (page.startsWith('post/')) {
        let id = parseInt(page.replace('post/', ''));
        content = <PostDetailPage id={id} />;
        break;
      }
      content = <NotFoundPage />;
      break;
  }
  return (
    <div className="App">
      <div>
        <button onClick={() => setPage('home')}>home</button>
        <button onClick={() => setPage('inbox')}>inbox</button>
        <button onClick={() => setPage('about')}>about</button>
      </div>
      <div>page: {page}</div>
      {content}
    </div>
  );
}

export default App;
