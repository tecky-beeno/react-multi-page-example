import React from 'react';
import Post from '../components/Post';
import './HomePage.css';

function HomePage(props: { setPage: (page: string) => void }) {
  return (
    <div className="HomePage">
      <h1>Home Page</h1>
      <div className="center">
        <div className="post-list">  
          <Post id={1} setPage={props.setPage} />
          <Post id={2} setPage={props.setPage} />
          <Post id={3} setPage={props.setPage} />
          <Post id={101} setPage={props.setPage} />
        </div>
      </div>
    </div>
  );
}
export default HomePage;
