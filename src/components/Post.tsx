import React from 'react';
import './Post.css';
function Post(props: { id: number; setPage: (page: string) => void }) {
  return (
    <div className="Post">
      <div>post {props.id}</div>
      <button onClick={() => props.setPage('post/' + props.id)}>comment</button>
    </div>
  );
}
export default Post;
